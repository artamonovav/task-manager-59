package ru.t1.artamonov.tm.service.model;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.artamonov.tm.api.repository.model.IProjectRepository;
import ru.t1.artamonov.tm.api.service.model.IProjectService;
import ru.t1.artamonov.tm.enumerated.Sort;
import ru.t1.artamonov.tm.enumerated.Status;
import ru.t1.artamonov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.artamonov.tm.exception.field.*;
import ru.t1.artamonov.tm.model.Project;
import ru.t1.artamonov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

@Service
@NoArgsConstructor
public class ProjectService implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @NotNull
    @PersistenceContext
    private EntityManager entityManager;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project add(@Nullable final Project model) {
        if (model == null) throw new ProjectNotFoundException();
        projectRepository.add(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project add(@Nullable final String userId, @Nullable final Project model) {
        if (model == null) throw new ProjectNotFoundException();
        model.setUser(entityManager.find(User.class, userId));
        projectRepository.add(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Collection<Project> add(@NotNull final Collection<Project> models) {
        if (models == null) throw new ProjectNotFoundException();
        for (@NotNull Project project : models) {
            projectRepository.add(project);
        }
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project changeProjectStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        project.setUser(entityManager.find(User.class, userId));
        return update(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        projectRepository.clearAll();
    }

    @Override
    public void clear(@Nullable final String userId) {
        projectRepository.clear(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Project project = new Project();
        project.setName(name);
        return add(userId, project);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(userId, project);
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.existsById(id);
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.existsByIdUserId(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.findAllUserId(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final Comparator<Project> comparator) {
        return projectRepository.findAll(comparator);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId, @Nullable final Comparator<Project> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.findAllUserId(userId, comparator);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.findAllUserId(userId, sort);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.findOneById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.findOneByIdUserId(userId, id);
    }

    @Override
    @SneakyThrows
    public long getSize() {
        return projectRepository.getSize();
    }

    @Override
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.getSizeUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project remove(@Nullable final Project model) {
        if (model == null) throw new ProjectNotFoundException();
        projectRepository.remove(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project remove(@Nullable final String userId, @Nullable final Project model) {
        if (model == null) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        projectRepository.removeByIdUserId(userId, model.getId());
        return model;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final Collection<Project> collection) {
        if (collection == null) throw new ProjectNotFoundException();
        for (@NotNull Project project : collection) {
            projectRepository.remove(project);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Project project = projectRepository.findOneById(id);
        projectRepository.removeById(id);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Project project = projectRepository.findOneByIdUserId(userId, id);
        projectRepository.removeByIdUserId(userId, id);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Collection<Project> set(@NotNull final Collection<Project> collections) {
        if (collections == null) return new ArrayList<>();
        clear();
        add(collections);
        return collections;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project update(@NotNull final Project model) {
        if (model == null) throw new ProjectNotFoundException();
        projectRepository.update(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        project.setUser(entityManager.find(User.class, userId));
        projectRepository.update(project);
        return project;
    }

}
