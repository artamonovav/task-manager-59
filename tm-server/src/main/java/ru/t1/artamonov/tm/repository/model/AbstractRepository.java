package ru.t1.artamonov.tm.repository.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.artamonov.tm.api.repository.model.IRepository;
import ru.t1.artamonov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

@NoArgsConstructor
public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    @Getter
    @PersistenceContext
    public EntityManager entityManager;

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

    public void set(@NotNull final Collection<M> models) {
        clearAll();
        models.forEach(this::add);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.remove(model);
    }

}
