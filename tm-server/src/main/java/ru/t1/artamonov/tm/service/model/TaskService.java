package ru.t1.artamonov.tm.service.model;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.artamonov.tm.api.repository.model.ITaskRepository;
import ru.t1.artamonov.tm.api.service.model.ITaskService;
import ru.t1.artamonov.tm.comparator.NameComparator;
import ru.t1.artamonov.tm.enumerated.Sort;
import ru.t1.artamonov.tm.enumerated.Status;
import ru.t1.artamonov.tm.exception.entity.TaskNotFoundException;
import ru.t1.artamonov.tm.exception.field.*;
import ru.t1.artamonov.tm.model.Task;
import ru.t1.artamonov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.*;

@Service
@NoArgsConstructor
public class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @NotNull
    @PersistenceContext
    private EntityManager entityManager;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task add(@Nullable Task model) {
        if (model == null) throw new TaskNotFoundException();
        taskRepository.add(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task add(@Nullable String userId, @Nullable Task model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new TaskNotFoundException();
        model.setUser(entityManager.find(User.class, userId));
        taskRepository.add(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Collection<Task> add(@NotNull Collection<Task> models) {
        if (models.isEmpty()) throw new TaskNotFoundException();
        for (@NotNull Task task : models) {
            taskRepository.add(task);
        }
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return update(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        taskRepository.clearAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        taskRepository.clear(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task create(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Task task = new Task();
        task.setUser(entityManager.find(User.class, userId));
        task.setName(name);
        return add(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task create(@Nullable String userId, @Nullable String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Task task = new Task();
        task.setUser(entityManager.find(User.class, userId));
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @Override
    public boolean existsById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.existsById(id);
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.existsByIdUserId(userId, id);
    }

    @Nullable
    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return taskRepository.findAllUserId(userId);
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable String userId, @Nullable Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return taskRepository.findAllUserId(userId, NameComparator.INSTANCE);
        return taskRepository.findAllUserId(userId, sort);
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable Comparator<Task> comparator) {
        if (comparator == null) return taskRepository.findAll(NameComparator.INSTANCE);
        return taskRepository.findAll(comparator);
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable String userId, @Nullable Comparator<Task> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return taskRepository.findAllUserId(userId, NameComparator.INSTANCE);
        return taskRepository.findAllUserId(userId, comparator);
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.findOneById(id);
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.findOneByIdUserId(userId, id);
    }

    @Override
    public long getSize() {
        return taskRepository.getSize();
    }

    @Override
    public long getSize(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return taskRepository.getSizeUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task remove(@Nullable Task model) {
        if (model == null) throw new TaskNotFoundException();
        taskRepository.remove(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task remove(@Nullable String userId, @Nullable Task model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new TaskNotFoundException();
        taskRepository.removeByIdUserId(userId, model.getId());
        return model;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable Collection<Task> collection) {
        if (collection == null) throw new TaskNotFoundException();
        for (@NotNull Task task : collection) {
            taskRepository.remove(task);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Task task = findOneById(id);
        taskRepository.removeById(id);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task removeById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Task task = findOneById(id);
        taskRepository.removeById(id);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Collection<Task> set(@NotNull Collection<Task> models) {
        if (models.isEmpty()) return new ArrayList<>();
        clear();
        add(models);
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task update(@NotNull Task model) {
        if (model == null) throw new TaskNotFoundException();
        taskRepository.update(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new IndexIncorrectException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        task.setUser(entityManager.find(User.class, userId));
        taskRepository.update(task);
        return task;
    }

}
