package ru.t1.artamonov.tm.exception.system;

public final class ArgumentIsNullException extends AbstractSystemException {

    public ArgumentIsNullException() {
        super("Error! Argument is null...");
    }

}
